// importar funciones de firebase
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getDatabase, ref, set, push, onValue, orderByKey, query  } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";
import { getAuth, onAuthStateChanged } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";

const firebaseConfig = {
    apiKey: "AIzaSyAODOa1VpV4S0FXe6RtIRL1YbBCcXEdz2c",
    authDomain: "tp-foro.firebaseapp.com",
    projectId: "tp-foro",
    databaseURL: "https://tp-foro-default-rtdb.firebaseio.com/",
    storageBucket: "tp-foro.appspot.com",
    messagingSenderId: "1041030864258",
    appId: "1:1041030864258:web:a483b209e2cd3c8fc7039b"
};

var UID;
var correo;

const app = initializeApp(firebaseConfig);
const database = getDatabase(app);
const auth = getAuth();



// Referencias al HTML

let mensajeRef = document.getElementById("Chat-escribir");
let botonRef = document.getElementById("boton-chat");
let chatRef = document.getElementById("chat-foro");

// 
onAuthStateChanged(auth, (user) => {                                                                                     //Aqui estamos utilizando la funcion "onAuthStateChanged" que nos sirve para determinar si el usuario inicio sesion o si el usuario no inicio sesion. Para esto utilizamos un IF//
    if (user) {
      
      botonRef.addEventListener("click", enviarMensaje);                                                                // Dentro del condicional si el usuario inicio sesion y hace click la funcion cargar informacion se ejecuta//

      const uid = user.uid;
      UID = uid
      console.log("uid: " + uid)

      const email = user.email;
      correo = email;
      console.log("correo: " + correo)
    
    } else {
      
      console.log("inicie sesion para escribir")
      botonRef.addEventListener("click", usuarion_no_habilitado);                                                              // Dentro del condicional si el usuario no inicio sesion y hace click la funcion usuario no registrado  se ejecuta, esta funcion //
    }
  });

//funcion cuando el usuario no esta habilitado 
function usuarion_no_habilitado(){
    window.location.href = './pages/Registrarse.html'; 
    alert("NO se esta registrado.")
}
  // Funcion par enviar mensaje 
function enviarMensaje(){
    let mensaje = mensajeRef.value;


    push(ref(database, "mensaje/"), {
        msg: mensaje
    });

    mensajeRef.value = "";
    console.log("Mensaje enviado.");
    location.reload()
}

const queryMensajes = query(ref(database, 'mensaje/'), orderByKey('msg'));
console.log(queryMensajes);


// Ver que enviaje se envio e imprimer en la pantalla 
onValue(queryMensajes, (snapshot) => {

    const lectura = snapshot.val();
    
    snapshot.forEach((childSnapshot) => {
      const childKey = childSnapshot.key;
      const chilData = childSnapshot.val().msg;

      chatRef.innerHTML += `
      <p>${correo}: ${childSnapshot.val().msg}</p>
  `;
    });
});