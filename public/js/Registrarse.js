import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import { getDatabase, ref, set } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";

const firebaseConfig = {
    apiKey: "AIzaSyAODOa1VpV4S0FXe6RtIRL1YbBCcXEdz2c",
    authDomain: "tp-foro.firebaseapp.com",
    projectId: "tp-foro",
    storageBucket: "tp-foro.appspot.com",
    messagingSenderId: "1041030864258",
    appId: "1:1041030864258:web:a483b209e2cd3c8fc7039b",
    databaseURL: "https://tp-foro-default-rtdb.firebaseio.com/"
  };

  const app = initializeApp(firebaseConfig);
  const auth = getAuth();
  const database = getDatabase(app)

// Referencias al HTML --- Registarse 

let correoRefReg = document.getElementById("Email-Registrarse");
let passRefReg = document.getElementById("Password-Registrarse");
let buttonRefReg = document.getElementById("Boton-Registrarse");
let NombreRefReg = document.getElementById("Nombre");
let CiudadRefReg = document.getElementById("Ciudad");

buttonRefReg.addEventListener("click", RegistroUser);

//Funcion para registarse

function RegistroUser(){


    console.log("Ingreso a la función altaUsuario().");

    if((correoRefReg.value != '') && (passRefReg.value != '') && (NombreRefReg.value != '') && (CiudadRefReg.value != '')){

        createUserWithEmailAndPassword(auth, correoRefReg.value, passRefReg.value, NombreRefReg.value, CiudadRefReg.value)
        .then((userCredential) => {
            // Signed in
            const user = userCredential.user;
            console.log("Usuario: " + user + " ID: " + user.uid);
            console.log("Creación de usuario.");

            let nombre = NombreRefReg.value
            let ciudad = CiudadRefReg.value


            set(ref(database, "usuarios/" + user.uid), {
                Nombre_Apellido: nombre,
                Ciudad: ciudad
            }).then(() => {
                window.location.href = "../index.html";
            })
            

        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
        });
    }
    else{
        alert("Revisar que los campos de usuario y contraseña esten completos.");
    }    

}